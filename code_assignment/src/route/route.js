import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Browse from './../pages/BrowsePage';
import Compare from './../pages/ComparePage';
import ProductDetail from './../pages/ProductDetailPage';

const Routerindex = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Browse} />
        <Route path="/compare" component={Compare} />
        <Route path="/productdetail/:data" component={ProductDetail} />
      </Switch>
    </Router>
  );
};

export default Routerindex;
