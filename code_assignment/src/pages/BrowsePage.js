import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from './../redux/action/productAction';
import './../assets/css/style.css';
import Card from './../components/commons/card';

let logo;

class Browse extends Component {
  constructor() {
    super();
    this.state = {
      dataInput: '',
      isSortByCreatedAt: false,
      isSortByPremium: false
    };
    this.dataInput = this.dataInput.bind(this);
    this.sortByCreatedAt = this.sortByCreatedAt.bind(this);
    this.callbackFunctionFilter = this.callbackFunctionFilter.bind(this);
    this.sortByPremium = this.sortByPremium.bind(this);
    this.getProductData = this.getProductData.bind(this);
    // this.premiumSort = this.premiumSort.bind(this);
  }

  componentWillMount() {
    this.props.fetchData();
  }

  sortByCreatedAt() {
    if (this.state.isSortByCreatedAt) {
      this.setState({ isSortByCreatedAt: false });
    } else {
      this.setState({ isSortByCreatedAt: true });
    }
  }

  sortByPremium() {
    if (this.state.isSortByPremium) {
      this.setState({ isSortByPremium: false });
    } else {
      this.setState({ isSortByPremium: true });
    }
  }

  dataInput(event) {
    let dataInput = event.target.value;
    this.setState({ dataInput });
  }

  callbackFunctionFilter({ props: { data } }) {
    const { dataInput } = this.state;
    const providerName =
      data.plan.insuranceProviderName === dataInput ||
      data.plan.planEligibility.serviceAreaIds[0] === dataInput;
    return providerName;
  }

  mappingData(data, index) {
    if (data.insuranceProviderId === 'RELIGARE_HEALTH') {
      logo = require('./../assets/images/Religare.png');
    } else if (data.insuranceProviderId === 'HDFC_ERGO') {
      logo = require('./../assets/images/HDFCErgo.png');
    } else {
      logo = require('./../assets/images/RelianceGeneral.png');
    }
    return <Card key={index} data={data} logo={logo} />;
  }

  // premiumSort(productData) {
  // const { isSortByPremium } = this.state;
  // const amount = _.get(productData, 'totalAmount.amount', 'not found');
  // console.log('premium ', productData[0].totalAmount.amount);

  //SORT BY PREMIUM AMOUNT
  // if (!isSortByPremium) {
  // if (productData !== 'not found') {
  //   productData.sort((a, b) => {
  //     console.log(true, -1);
  //     return b.totalAmount.amount - a.totalAmount.amount;
  //   });

  //   if (this.state.dataInput === '') {
  //     // return productData.map(this.mappingData);
  //     return <div>Hello</div>;
  //   }

  //   if (this.state.dataInput !== '') {
  //     return productData
  //       .map(this.mappingData)
  //       .filter(this.callbackFunctionFilter);
  //   }
  // }
  // } else if (isSortByPremium) {
  // if (productData !== 'not found') {
  //   productData.sort((a, b) => {
  //     return b.totalAmount.amount - a.totalAmount.amount;
  //   });

  //   if (this.state.dataInput === '') {
  //     return productData.map(this.mappingData);
  //   }

  //   if (this.state.dataInput !== '') {
  //     return productData
  //       .map(this.mappingData)
  //       .filter(this.callbackFunctionFilter);
  //   }
  // }
  // }
  // }

  getProductData() {
    const { data } = this.props.product;
    const { isSortByPremium, isSortByCreatedAt } = this.state;
    const productData = _.get(data, 'content', 'not found');

    // const { product } = this.props;
    //SORT BY PREMIUM AMOUNT
    if (!this.state.isSortByPremium) {
      console.log(this.state.isSortByCreatedAt);
      if (productData !== 'not found') {
        productData.sort((a, b) => {
          return a.totalAmount.amount - b.totalAmount.amount;
        });

        if (this.state.dataInput === '') {
          return productData.map(this.mappingData);
        }

        if (this.state.dataInput !== '') {
          return productData
            .map(this.mappingData)
            .filter(this.callbackFunctionFilter);
        }
      }
    } else if (isSortByPremium) {
      console.log('not ok');
      if (productData !== 'not found') {
        productData.sort((a, b) => {
          return b.totalAmount.amount - a.totalAmount.amount;
        });

        if (this.state.dataInput === '') {
          return productData.map(this.mappingData);
        }

        if (this.state.dataInput !== '') {
          return productData
            .map(this.mappingData)
            .filter(this.callbackFunctionFilter);
        }
      }
    }
    //SORT BY CREATED AT
    // if (!isSortByCreatedAt) {
    //   if (productData !== 'not found') {
    //     productData.sort((a, b) => {
    //       const sort = a.plan.createdAt - b.plan.createdAt;

    //       if (sort < 0) {
    //         return -1;
    //       } else if (sort === 0) {
    //         return 0;
    //       } else {
    //         return 1;
    //       }
    //     });

    //     if (this.state.dataInput === '') {
    //       return productData.map(this.mappingData);
    //     }

    //     if (this.state.dataInput !== '') {
    //       return productData
    //         .map(this.mappingData)
    //         .filter(this.callbackFunctionFilter);
    //     }
    //   }
    // } else if (isSortByCreatedAt) {
    //   if (productData !== 'not found') {
    //     productData.sort((a, b) => {
    //       return b.plan.createdAt - a.plan.createdAt;
    //     });

    //     if (this.state.dataInput === '') {
    //       return productData.map(this.mappingData);
    //     }

    //     if (this.state.dataInput !== '') {
    //       return productData
    //         .map(this.mappingData)
    //         .filter(this.callbackFunctionFilter);
    //     }
    //   }
    // }
  }

  render() {
    console.log(this.props);

    return (
      <div>
        <div>
          <input
            name="filter"
            type="text"
            placeholder="input by insurance provider or  Service Area Ids "
            onChange={this.dataInput}
          />
        </div>
        <div>
          {/* <button onClick={this.sortByCreatedAt}>sort by date created </button> */}
          <button onClick={this.sortByPremium}>
            sort by amount of premium at
          </button>
        </div>
        <div className="listWrapper">
          {this.getProductData()}
          {/* <nav>
        <ul>
          <Link to="/">Browser</Link>
          <Link to="/productdetail">Product Detail</Link>
          <Link to="compare">Compare</Link>
        </ul>
      </nav> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { product: state };
};

export default connect(
  mapStateToProps,
  actions
)(Browse);
