import React from 'react';
import './../../assets/css/style.css';
import { Link } from 'react-router-dom';

const Card = props => {
  const {
    logo,
    data,
    data: { plan }
  } = props;

  return (
    <div className="card">
      <div className="imageWrapper">
        <img src={logo} alt="logo" className="image" />
      </div>
      <div className="cardTitle" key={plan.name}>
        {plan.planName}
      </div>
      <div className="cardDescription" key={plan.insuranceProviderName}>
        <p>{plan.insuranceProviderName}</p>
      </div>

      <div className="cardFooter" key={data.key}>
        <div key={data.sumInsured}>
          <label>
            <p> insured: {data.sumInsured}</p>
          </label>
        </div>
        <div key={data.totalAmount.amount}>
          <label>
            <p> premium : {data.totalAmount.amount}</p>
          </label>
        </div>
        <div key={data.key}>
          <Link to={'/productdetail/' + { data }}>Details</Link>
        </div>
      </div>
    </div>
  );
};

export default Card;
