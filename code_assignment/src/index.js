import React from 'react';
import ReactDOM from 'react-dom';
// import Browse from './pages/BrowsePage';
import Routerindex from './route/route';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './../src/redux/store/store';

const Apps = () => {
  return (
    <Provider store={store}>
      <Routerindex />
    </Provider>
  );
};

ReactDOM.render(<Apps />, document.getElementById('root'));
registerServiceWorker();
