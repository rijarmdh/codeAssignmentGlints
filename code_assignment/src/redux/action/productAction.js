import jsonData from './../../assets/json/data.json';
import { FETCH_SUCCESS } from './../consts/const';

export const fetchData = () => {
  return {
    type: FETCH_SUCCESS,
    payload: jsonData
  };
};
