import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import productReducer from './../reducers/productReducer';

const middleware = applyMiddleware(ReduxThunk, logger);
const store = createStore(productReducer, middleware);

export default store;
