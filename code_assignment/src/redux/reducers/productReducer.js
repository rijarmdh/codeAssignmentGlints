import { FETCH_SUCCESS } from './../consts/const';

const INITIAL_STATE = {
  data:''
};

const productReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      state = { ...state, data: action.payload };
      break;
    default:
      break;
  }
  return state;
};

export default productReducer;
